SOURCE_FOLDER ?= source

LIB_FOLDER ?= lib

REPO_URL ?= git@gitlab.oit.duke.edu:dws-contributes/duke-scholars-drupal/duke-scholars-view-react.git

clone:
	@echo @echo "cloning repo into source folder"
	git clone $(REPO_URL) -b develop source
	cd $(SOURCE_FOLDER) && npm install

start:
	@echo "running `npm run-script start`"
	cd $(SOURCE_FOLDER) && npm run-script start

build:
	@echo "building production ready react files. Please make sure this is running off the master branch"
	rm -rf $(LIB_FOLDER)/*
	cd $(SOURCE_FOLDER) && npm run-script build && rsync -r build/ ../$(LIB_FOLDER)/
	sh scripts/generate-libraries-yml.sh

