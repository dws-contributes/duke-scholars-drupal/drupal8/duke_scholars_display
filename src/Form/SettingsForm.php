<?php

namespace Drupal\duke_scholars_display\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Duke Scholars REACT Display settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'duke_scholars_display_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['duke_scholars_display.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['pagination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Pagination'),
      '#default_value' => $this->config('duke_scholars_display.settings')->get('pagination'),
    ];

    $form['items_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of items per page'),
      '#default_value' => $this->config('duke_scholars_display.settings')->get('items_per_page')
    ];

    $form['display_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Label for react components'),
      '#default_value' => $this->config('duke_scholars_display.settings')->get('display_label'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('duke_scholars_display.settings')
      ->set('pagination', $form_state->getValue('pagination'))
      ->set('items_per_page', $form_state->getValue('items_per_page'))
      ->set('display_label', $form_state->getValue('display_label'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
