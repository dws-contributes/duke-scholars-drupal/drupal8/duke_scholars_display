#!/bin/bash

MAIN_JS=$(find lib/static/js -name 'main*.js' -exec echo {} \;);
RUNTIME_JS=$(find lib/static/js -name 'runtime*.js' -exec echo {} \;);
BUNDLE_JS=$(find lib/static/js -name '2.*.js' -exec echo {} \;);

rm ./duke_scholars_display.libraries.yml
cp ./template.libraries.txt duke_scholars_display.libraries.yml

sed -i '' "s#mainjs#$MAIN_JS#g" duke_scholars_display.libraries.yml
sed -i '' "s#second#$BUNDLE_JS#g" duke_scholars_display.libraries.yml
sed -i '' "s#runtime#$RUNTIME_JS#g" duke_scholars_display.libraries.yml
for FILE in $(find lib/static/css -name '*.css';); do
  echo "      ${FILE}: {}" >> duke_scholars_display.libraries.yml
done
