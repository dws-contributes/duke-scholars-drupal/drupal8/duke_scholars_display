## Duke Scholars Display module
Provides React-powered components for displaying additional information for Scholars Profiles.

#### Installation
If using composer, add drupal_scholars_display in composer.json:
````
"repositories": {
  ...
  "duke/drupal_scholars_display": {
    "type": "vcs",
    "url": "https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke_scholars_display.git"
  },
},  
"require": {
  ...
  "duke/drupal_scholars_display": "^2.0.0",
````

#### The following fields are displayed as components:
  - overview
  - publications
  - grants
  - courses
  - news feeds 
  - education
  - professional activities
  - academic positions
  - research areas
  - addresses
  - webpages
  - artistic events
  - past appointments
  - geographical focus
  - gifts
  - licenses

#### Configuration  
Go to Drupal Admin -> Configuration -> Web services -> Duke Scholars Display Settings. You can customize the following options:
- Enabling pagination for a component (ex: paginate publications)
- Number of item per page inside a component
- Displaying a label for a component

## How to work on this repo
- spin up local copy with `make clone`
- work on repo by running `make start`
- once done working on react, run `make build`
- commit your changes and deploy

### Makefile commands and explanations

- `clone`
    - clones the repo into the `source` directory
- `start`
    - fires off the `npm run-script start` process for development
- `build`
    - builds out prod ready assets. These get pulled into the Drupal module.
